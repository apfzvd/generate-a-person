import { graphql, withApollo } from 'react-apollo'
import gql from 'graphql-tag'
import React, { Component } from 'react';

class Person extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    const {
      loading,
      getPerson
    } = this.props.data

    return (
      <h2>
        {
          (getPerson && !loading) ? (
            `${getPerson.name} ${getPerson.surname} - Region: ${getPerson.region}`
          ) : 'Loading...'
        }
      </h2>
    )
  }
}

const GENERATE_PERSON = gql`
  query getAllGenders ($gender: String!) {
    getPerson(gender: $gender) {
      name,
      surname,
      region
    }
  }
`

const options = {
  options: ({ gender }) => ({
    variables: { gender },
    fetchPolicy: 'network-only'
  })
}

export default graphql(GENERATE_PERSON, options) (Person)
