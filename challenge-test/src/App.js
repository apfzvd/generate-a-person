import { graphql, withApollo } from 'react-apollo'
import gql from 'graphql-tag'
import React, { Component } from 'react';
import Person from './components/Person'

class App extends Component {

  constructor (props) {
    super(props)
    this.changeName = this.changeName.bind(this)
    this.state = {
      gender: 'female'
    }
  }

  changeName (e) {
    this.setState({
      gender: e.target.value
    })
  }

  render() {
    const {
      data
    } = this.props

    return (
      <div className="App" style={{textAlign: 'center'}}>
        <h1 className="App-title">Generate a person:</h1>
        <Person gender={this.state.gender} />
        Female <input name="gender" type="radio" value="female" onChange={this.changeName} />
        <br/>
        Male <input name="gender" type="radio" value="male" onChange={this.changeName} />
      </div>
    );
  }
}

export default withApollo(App);
