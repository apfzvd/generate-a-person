const uri = 'https://uinames.com/api/'
import fetch from 'node-fetch'

const resolvers = {
  Query: {
    getPerson(root, args, context) {
      return fetch(`${uri}?gender=${args.gender}`)
        .then(res => {return res.json()})
        .then(json => {
          console.log(json)
          return {
            name: json.name,
            surname: json.surname,
            region: json.region
          }
        })
    },
  }
};

export default resolvers;
