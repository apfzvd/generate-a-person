const typeDefs = `
type Person {
  name: String,
  surname: String,
  region: String
}

# the schema allows the following query:
type Query {
  getPerson(gender: String!): Person
}

# we need to tell the server which types represent the root query
# and root mutation types. We call them RootQuery and RootMutation by convention.
schema {
  query: Query
}
`;

export default typeDefs;
