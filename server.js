import express from 'express'
import bodyParser from 'body-parser'
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express'
// import { util } from 'util'
// const parseXML = util.promisify(require('xml2js').parseString)
import { makeExecutableSchema } from 'graphql-tools';
import typeDefs from './graphql/types'
import resolvers from './graphql/resolvers'

const executableSchema = makeExecutableSchema({
  typeDefs,
  resolvers,
})

const app = express()

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

// bodyParser is needed just for POST.
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema: executableSchema }))
app.post('/api', bodyParser.json(), graphqlExpress({ schema: executableSchema }))
app.get('/graphiql', graphiqlExpress({ endpointURL: '/graphql' })) // if you want GraphiQL enabled

app.listen(8001, (res) => console.log('Listening...'));
